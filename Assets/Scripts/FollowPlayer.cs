using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
  public GameObject player;
  private Vector3 thirdViewPos = new Vector3(0f, 5f, -7f);
  private Vector3 thirdViewRot = new Vector3(30f, 0f, 0f);
  private Vector3 seatViewPos = new Vector3(0f, 1.8f, 1f);
  private Vector3 seatViewRot = new Vector3(0f, 0f, 0f);

  private bool isFps = false;

  // Start is called before the first frame update
  void Start()
  {

  }

  // Update is called once per frame
  void LateUpdate()
  {
    Vector3 playerRot = new Vector3(player.transform.rotation.x, player.transform.rotation.y, player.transform.rotation.z);

    if (Input.GetKeyDown(KeyCode.Alpha1))
    {
      isFps = true;
    }
    else if (Input.GetKeyDown(KeyCode.Alpha2))
    {
      isFps = false;
    }

    if (!isFps)
    {
      transform.position = player.transform.position + thirdViewPos;
      transform.eulerAngles = thirdViewRot;
    }
    else
    {
      transform.position = player.transform.position + seatViewPos;
      transform.eulerAngles = seatViewRot;
      transform.Rotate(playerRot);
    }
  }
}
