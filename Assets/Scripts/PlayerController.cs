using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
  [SerializeField]
  private float speed;
  [SerializeField]
  private float turnSpeed;
  private float horizontalInput;
  private float forwardInput;

  // Update is called once per frame
  void FixedUpdate()
  {
    horizontalInput = Input.GetAxis("Horizontal");
    forwardInput = Input.GetAxis("Vertical");

    // Moves the car forward based on vertical input
    transform.Translate(Vector3.forward * Time.deltaTime * speed * forwardInput);
    // Rotates the car based on horizontal input
    transform.Rotate(Vector3.up, Time.deltaTime * turnSpeed * horizontalInput);
  }
}
