﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube : MonoBehaviour
{
  public MeshRenderer Renderer;
  public Vector3 newPosition;
  public Color color;

  public float scale;
  private float speedRotation;

  void Start()
  {
    speedRotation = Random.Range(10, 15);

    transform.position = newPosition;
    transform.localScale = Vector3.one * scale;

    Material material = Renderer.material;
    material.color = color;
  }

  void Update()
  {
    transform.Rotate(speedRotation * Time.deltaTime, speedRotation * Time.deltaTime, speedRotation * Time.deltaTime);
  }
}
